Package to test `npm install` and `npm publish` on self managed
Make sure to:

1) Clone the project
2) Change url in to match your instance url under publishConfig 

``` 
"publishConfig": {
    "@sahbabou:registry":"http:/gitlab.ahbabou.com/api/v4/74/packages/npm/"
  }
  
```
3) Update your ~/.npmrc with the appropriate project ID  